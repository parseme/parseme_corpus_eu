README
------
This is the README file for the PARSEME verbal multiword expressions (VMWEs) corpus for Basque, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Basque data is the result of an update and an extension of the Basque part of the [PARSEME 1.2 corpus](http://hdl.handle.net/11234/1-3367).
 
The raw corpus released in edition 1.2 can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Source corpora
-------
All annotated data come from one of these sources (subcorpora):
* UD: 6621 sentences, the whole [Universal Dependencies treebank for Basque](https://universaldependencies.org/treebanks/eu_bdt/index.html), which contains texts from newspapers
* Elhuyar: 4537 sentences taken from the [Elhuyar Web Corpora](https://www.elhuyar.eus/en/site/projects/hizkuntza-baliabideak-euskal-gizartearentzat/dictionaries-and-corpora), which contains texts collected by web crawling (mainly administrative texts)

The source subcorpus can be recognized by the sentence identifiers (source_sent_id), where UD and Elhuyar are distinguished.

Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Information about some columns:
* LEMMA (column 3): Available. Automatically annotated.
* UPOS (column 4): Available. Automatically annotated for Elhuyar manually annotated for UD.
* FEATS (column 6): Available. Automatically annotated.
* HEAD and DEPREL (columns 7 and 8): Available. Automatically annotated for Elhuyar, manually annotated for UD. The inventory is [Universal Dependency Relations](https://universaldependencies.org/u/dep/).
* MISC (column 10): No-space information available. Automatically annotated.
* PARSEME:MWE (column 11): Manually annotated by a single annotator per file. The following VMWE categories are annotated: VID, LVC.full and LVC.cause.

Lemmas and morphological features for the UD corpus were automatically annotated by [Eustagger](http://ixa.eus/node/4450). POS tags and dependency relations in the UD corpus were first annotated based on a Basque tagset and then automatically converted to UD tags. The Elhuyar corpus was entirely annotated by the [Mate parser](https://code.google.com/archive/p/mate-tools/) and then reannotated for morphosyntax (LEMMA to DEP\_REL) with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) (`basque-bdt-ud-2.10-220711` model).

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

<!--
Companion raw corpus
--------------------
The manually annotated corpus, described above, is accompanied by a large "raw" corpus (meant for automatic discovery of VMWEs), in which VMWEs are not annotated and morphosyntax is automatically tagged. Its characteristics are the following:
* size (uncompressed): 1.3GB
* sentences: 1,327,630
* tokens: 21,268,325
* tokens/sentence: 16.02
* format: [CoNLL-U](https://universaldependencies.org/format.html)
* source: the [Berria](https://www.berria.eus/) Basque newspaper 
* genre: news
* morpho-syntactic tagging: automatically annotated by using [UDPipe](http://ufal.mff.cuni.cz/udpipe), based on the [Basque model](https://ufal.mff.cuni.cz/udpipe/models).
-->

Authors
----------
All changes on VMWE annotations (column 11) for this version were performed by Uxoa Iñurrieta. Tecnical support for the processing of the raw corpus was given by Arantxa Otegi.

Previous annotators (listed in alphabetical order): Itziar Aduriz, Ainara Estarrona, Itziar Gonzalez-Dios, Antton Gurrutxaga, Uxoa Iñurrieta and Ruben Urizar.

License
----------
The full dataset is licensed under Creative Commons Non-Commercial Share-Alike 4.0 [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Contact
----------
usoa.inurrieta@ehu.eus

Known issues
------------
* The `file_path` in the `source_sent_id` field of the UD corpus cannot be traced to the original treebank, because the corpus consists of a concatenation of all train, test and dev files. The fields could be retrieved by matching the `text` fields.

Future work
-----------
The Elhuyar part of the corpus (EU\_Elhuyar_updated.cupt file) had automatic annotations of morphosyntax (LEMMA to DEP\_REL) and it was automatically re-annotated using UDPipe with the `basque-bdt-ud-2.10-220711` model. 
In the UD part of the corpus the morphosyntactic annotation is manual (stems from UD\_Basque-BDT). Unfortunately, the PARSEME corpus (EU\_UD-updated.cupt) does not containt identifiers of the sources sentences from UD\_Basque-BDT (see Known Issues above), therefore synchronising morphosyntax in the two corpora was not straightforward. This should be done in the next versions.

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.2 version are the following:
      - updating the morphosyntactic annotation in the Elhuyar part of the corpus, using [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) and the `basque-bdt-ud-2.10-220711` model.
- **2020-07-09**:
  - [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.
  - Changes with respect to the 1.1 version are the following:
    - updating the morphosyntactic annotation, so that all tags are compatible with Universal Dependencies, since part of the previous corpus contained tags from another tagset
    - fixing known errors in the previous version, mainly based on the annotation undertaken for the [Multilingual corpus of literal occurrences of multiword expressions](https://lindat.mff.cuni.cz/repository/xmlui/handle/11372/LRT-2966)
    - providing a companion raw corpus, automatically annotated for morphosyntax
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.

